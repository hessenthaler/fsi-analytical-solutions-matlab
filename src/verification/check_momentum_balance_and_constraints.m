function [] = check_momentum_balance_and_constraints(d)
    % check momentum balance?
    if ~d.balanceEqn
        return
    end
    fprintf('Checking whether balance equations and coupling conditions are satisfied:\n\n');
    % variable to store mismatch
    valF                = 0.0;
    valS                = 0.0;
    valIt               = 0.0;
    valIv               = 0.0;
    % loop over random points
    for i = 1:d.nSamples
        % sample time
        t               = d.T * rand(1, 1);
        %% fluid
        % sample fluid space
        x               = randpt(d, 'fluid', 'interior');
        % evaluate fluid terms
        vf              = evaluate_cell_handles(d, d.func_vf, x, t);
        wf              = evaluate_cell_handles(d, d.func_wf, x, t);
        dvfdt           = ddt(d, d.func_vf, x, t);
        gradvf          = grad(d, d.func_vf, x, t, false);
        visc_vf         = viscous_stress_term(d, d.func_vf, x, t);
        gradpf          = grad(d, d.func_pf, x, t, true);
        % check fluid momentum balance
        fluideqn        = norm(d.rhof * dvfdt * d.dynamicF + d.nonlinear * d.rhof * gradvf * (vf - wf) - d.muf * visc_vf + gradpf, 2);
        %% solid
        % sample solid space
        x               = randpt(d, 'solid', 'interior');
        % evaluate solid terms
        dvsdt           = ddt(d, d.func_vs, x, t);
        % check solid momentum balance
        if d.nonlinear
            divP        = divPK(d, d.func_us, d.func_ps, x, t);
            solideqn    = norm(d.rhos * dvsdt * d.dynamicS - divP, 2);
        else
            visc_us     = viscous_stress_term(d, d.func_us, x, t);
            gradps      = grad(d, d.func_ps, x, t, true);
            solideqn    = norm(d.rhos * dvsdt * d.dynamicS - d.mus * visc_us + gradps, 2);
        end
        %% interface
        % sample coupling space
        x               = randpt(d, 'interface', 'interior');
        % evaluate velocities
        vf              = evaluate_cell_handles(d, d.func_vf, x, t);
        vs              = evaluate_cell_handles(d, d.func_vs, x, t);
        % evaluate stress tensors
        stressF         = cauchy_stress_tensor(d, d.func_vf, d.func_pf, d.muf, x, t);
        if d.nonlinear
            stressS     = first_pk_stress_tensor(d, d.func_us, d.func_ps, x, t);
        else
            stressS     = cauchy_stress_tensor(d, d.func_us, d.func_ps, d.mus, x, t);
        end
        % get surface normals
        if (d.nd == 3)
            nF          = [x(1); x(2); 0.0] ./ norm([x(1); x(2); 0.0], 2);
        else
            nF          = [0.0;  x(2)]      ./ norm([0.0;  x(2)],      2);
        end
        nS              = -nF;
        % compute surface tractions
        tractionF       = stressF * nF;
        tractionS       = stressS * nS;
        % check traction condition
        tractioneqn     = norm(tractionF+tractionS, 2);
        % check velocity condition
        velocityeqn     = norm(vf-vs, 2);
        %% add to mismatch
        valF            = valF  + fluideqn;
        valS            = valS  + solideqn;
        valIt           = valIt + tractioneqn;
        valIv           = valIv + velocityeqn;
    end
    % compute and print average mismatch
    valF                = valF  / double(d.nSamples);
    valS                = valS  / double(d.nSamples);
    valIt               = valIt / double(d.nSamples);
    valIv               = valIv / double(d.nSamples);
    fprintf(sprintf('    Fluid balance equation error: %.16g\n', valF));
    fprintf(sprintf('    Solid balance equation error: %.16g\n', valS));
    fprintf(sprintf('    Traction coupling error:      %.16g\n', valIt));
    fprintf(sprintf('    Velocity coupling error:      %.16g\n', valIv));
    % check whether tolerance is satisfied
    tolF                = d.balanceFtol;
    tolS                = d.balanceStol;
    tolT                = d.constrTtol;
    tolV                = d.constrVtol;
    satisfied           = ~((valF > tolF) || (valS > tolS) || (valIt > tolT) || (valIv > tolV));
    if ~satisfied
        warning('Check balance / constraint equations but consider scaling of solutions and stresses');
    end
    fprintf('\n');
end