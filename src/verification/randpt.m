% This function creates a random point in the fluid or solid.
% Points can either be in the interior or on a specific boundary.
%
%   domain  :   'fluid' or 'solid'
%
%   bndry   :   'interior' / 'inlet' / 'outlet'
%               'inner wall' / 'outer wall'
%
function X = randpt(d, domain, bndry)

    % creating the random radius, angle and z parameters
    if strcmpi(domain, 'fluid')
        if (d.nd == 3)
            r       = d.Hi * rand(1, 1);
            theta   = 2.0 * pi * rand(1, 1);
            x       = r * cos(theta);
            y       = r * sin(theta);
            z       = d.L * rand(1, 1);
        else
            x       = d.L  * rand(1, 1);
            y       = d.Hi * rand(1, 1);
        end
    elseif strcmpi(domain, 'solid')
        if (d.nd == 3)
            r       = d.Hi + (d.Ho - d.Hi) * rand(1, 1);
            theta   = 2.0 * pi * rand(1, 1);
            x       = r * cos(theta);
            y       = r * sin(theta);
            z       = d.L * rand(1, 1);
        else
            x       = d.L * rand(1,1);
            y       = d.Hi + (d.Ho - d.Hi) * rand(1, 1);
        end
    elseif strcmpi(domain, 'interface')
        if (d.nd == 3)
            r       = d.Hi;
            theta   = 2.0 * pi * rand(1, 1);
            x       = r * cos(theta);
            y       = r * sin(theta);
            z       = d.L * rand(1, 1);
        else
            x       = d.L * rand(1, 1);
            y       = d.Hi;
        end
    else
        error('Function randpt expects domain fluid, solid or interface');
    end

    % snapping to the appropriate boundary (if necessary)
    if (strcmpi(bndry,'interior') == 1)
    elseif (strcmpi(bndry,'inlet') == 1)
        if (d.nd == 3)
            z   = 0.0;
        else
            x   = 0.0;
        end
    elseif (strcmpi(bndry,'outlet') == 1)
        if (d.nd == 3)
            z   = d.L;
        else
            x   = d.L;
        end
    elseif (strcmpi(bndry,'inner wall') == 1) && (strcmpi(domain,'fluid') == 1)
        if (d.nd == 3)
            r   = d.Hi;
            x   = r * cos(theta);
            y   = r * sin(theta);
        else
            y   = d.Hi;
        end
    elseif (strcmpi(bndry,'inner wall') == 1) && (strcmpi(domain,'solid') == 1)
        error('Function randpt expects domain boundary inner wall for fluid only.');
    elseif (strcmpi(bndry,'outer wall') == 1) && (strcmpi(domain,'solid') == 1)
        if (d.nd == 3)
            r   = d.Ho;
            x   = r * cos(theta);
            y   = r * sin(theta);
        else
            y   = d.Ho;
        end
    elseif (strcmpi(bndry,'outer wall') == 1) && (strcmpi(domain,'fluid') == 1)
        error('Function randpt expects domain boundary outer wall for solid only.');
    else
        error('Function randpt expects domain boundary: inlet, outlet, inner wall, outer wall, interior');
    end

    % creating the point
    if (d.nd == 3)
        X   = [x; y; z];
    else
        X   = [x; y];
    end
end