function result = divgrad(d, func,x,t)
    eps     = 1e-4;
    nd      = d.nd;
    id      = eye(nd);
    result  = zeros(nd, 1);
    for i = 1:nd
        Mp          = grad(d, func, x+eps*id(:,i), t, false);
        Mm          = grad(d, func, x-eps*id(:,i), t, false);
        result(:)   = result(:) + (Mp(:, i) - Mm(:, i)) / (2.0 * eps);
    end
end