function result = divPK(d, func, funcp, x, t)
    eps     = 1e-6;
    nd      = d.nd;
    id      = eye(nd);
    result  = zeros(nd, 1);
    for i = 1:nd
        Mp          = first_pk_stress_tensor(d, func, funcp, x+eps*id(:,i), t);
        Mm          = first_pk_stress_tensor(d, func, funcp, x-eps*id(:,i), t);
        result(:)   = result(:) + (Mp(:, i) - Mm(:, i)) / (2.0 * eps);
    end
end