function result = ddt(d, func, x, t)
    eps     = 1e-6;
    Mp      = evaluate_cell_handles(d, func, x, t+eps);
    Mm      = evaluate_cell_handles(d, func, x, t-eps);
    result  = (Mp - Mm) / (2.0 * eps);
end