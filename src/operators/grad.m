function result = grad(d, func, x, t, scalarVal)
    eps                     = 1e-6;
    nd                      = d.nd;
    id                      = eye(nd);
    if scalarVal
        result              = zeros(nd, 1);
    else
        result              = zeros(nd, nd);
    end
    for i = 1:nd
        xp                  = x + eps * id(:, i);
        xm                  = x - eps * id(:, i);
        if scalarVal
            if (nd == 3)
                Mp              = d.func_pf(d, xp(1), xp(2), xp(3), t);
                Mm              = d.func_pf(d, xm(1), xm(2), xm(3), t);
            else
                Mp              = d.func_pf(d, xp(1), xp(2), 0.0, t);
                Mm              = d.func_pf(d, xm(1), xm(2), 0.0, t);
            end
            result(i)       = (Mp - Mm) / (2.0 * eps);
        else
            Mp              = evaluate_cell_handles(d, func, xp, t);
            Mm              = evaluate_cell_handles(d, func, xm, t);
            result(:, i)    = (Mp - Mm) / (2.0 * eps);
        end
    end
end