function result = double_dot(A, B)
    nd          = size(A, 1);
    if (nd ~= size(A, 2)) || (nd ~= size(B, 1)) || (nd ~= size(B, 2))
        error('Function double_dot takes two arguments of same (square) dimension');
    end
    result      = sum(sum(A .* B));
end