function export_data(d)
    % export coordinates
    save([d.exportFolder, 'domainF.txt'], '-struct', 'd', 'xyzF', '-ascii', '-double');
    save([d.exportFolder, 'domainS.txt'], '-struct', 'd', 'xyzS', '-ascii', '-double');
    % export velocity, displacement, pressure for each time point
    for tIdx = 1:length(d.t)
        % set up arrays
        velF    = [d.vfx(:, tIdx), d.vfy(:, tIdx), d.vfz(:, tIdx)];
        velS    = [d.vsx(:, tIdx), d.vsy(:, tIdx), d.vsz(:, tIdx)];
        welF    = [d.wfx(:, tIdx), d.wfy(:, tIdx), d.wfz(:, tIdx)];
        dispS   = [d.usx(:, tIdx), d.usy(:, tIdx), d.usz(:, tIdx)];
        presF   = d.pf(:, tIdx);
        presS   = d.ps(:, tIdx);
        % export data
        save([d.exportFolder, 'velocityF-',         int2str(tIdx), '.txt'], 'velF',  '-ascii', '-double');
        save([d.exportFolder, 'velocityS-',         int2str(tIdx), '.txt'], 'velS',  '-ascii', '-double');
        save([d.exportFolder, 'domainVelocityF-',   int2str(tIdx), '.txt'], 'welF',  '-ascii', '-double');
        save([d.exportFolder, 'displacementS-',     int2str(tIdx), '.txt'], 'dispS', '-ascii', '-double');
        save([d.exportFolder, 'pressureF-',         int2str(tIdx), '.txt'], 'presF', '-ascii', '-double');
        save([d.exportFolder, 'pressureS-',         int2str(tIdx), '.txt'], 'presS', '-ascii', '-double');
    end
end