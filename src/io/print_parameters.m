function print_parameters(d)

    LogicalStr  = {'false', 'true'};
    newline     = '\n';

    fprintf(newline);
    fprintf(sprintf('Parameters for this run:\n'));
    fprintf(newline);
    fprintf(sprintf('    user-defined mesh:         %s\n', LogicalStr{d.userMesh + 1}));
    fprintf(newline);
    fprintf(sprintf('    number of dimensions:      %i\n', d.nd));
    fprintf(sprintf('    nonlinear:                 %s\n', LogicalStr{d.nonlinear + 1}));
    fprintf(newline);
    fprintf(sprintf('    angular frequency:         %.3f\n', d.w));
    fprintf(sprintf('    time domain:               [0.000, %.3f]\n', d.T));
    fprintf(newline);
    fprintf(sprintf('    fluid:\n'));
    fprintf(newline);
    fprintf(sprintf('        domain:               (x, y) in [0.000, %.3f] x [0.000, %.3f]\n', d.L, d.Hi));
    fprintf(sprintf('        dynamic:               %s\n', LogicalStr{d.dynamicF + 1}));
    fprintf(sprintf('        pressure amplitude:    %.3f\n', d.Pf));
    fprintf(sprintf('        density:               %.3f\n', d.rhof));
    fprintf(sprintf('        viscosity:             %.3f\n', d.muf));
    fprintf(newline);
    fprintf(sprintf('    solid:\n'));
    fprintf(newline);
    fprintf(sprintf('        domain:               (x, y) in [0.000, %.3f] x [%.3f, %.3f]\n', d.L, d.Hi, d.Ho));
    fprintf(sprintf('        dynamic:               %s\n', LogicalStr{d.dynamicS + 1}));
    fprintf(sprintf('        pressure amplitude:    %.3f\n', d.Ps));
    fprintf(sprintf('        density:               %.3f\n', d.rhos));
    fprintf(sprintf('        stiffness parameter:   %.3f\n', d.mus));
    fprintf(newline);
    fprintf(sprintf('    interface:\n'));
    fprintf(newline);
    fprintf(sprintf('        domain size:           x in [0.000, %.3f], y = %.3f\n', d.L, d.Hi));
    fprintf(newline);

end