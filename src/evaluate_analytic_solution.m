function d = evaluate_analytic_solution(d)

    %% allocate variables
    d.vfx   = zeros(length(d.xf), length(d.t));
    d.vfy   = zeros(length(d.xf), length(d.t));
    d.vfz   = zeros(length(d.xf), length(d.t));
    d.wfx   = zeros(length(d.xf), length(d.t));
    d.wfy   = zeros(length(d.xf), length(d.t));
    d.wfz   = zeros(length(d.xf), length(d.t));
    d.pf    = zeros(length(d.xf), length(d.t));
    d.usx   = zeros(length(d.xs), length(d.t));
    d.usy   = zeros(length(d.xs), length(d.t));
    d.usz   = zeros(length(d.xs), length(d.t));
    d.vsx   = zeros(length(d.xs), length(d.t));
    d.vsy   = zeros(length(d.xs), length(d.t));
    d.vsz   = zeros(length(d.xs), length(d.t));
    d.ps    = zeros(length(d.xs), length(d.t));

    %% evaluate analytic solution
    for ti = 1:length(d.t)
        d.vfx(:, ti)   = d.func_vfx(d, d.xf, d.yf, d.zf, d.t(ti));
        d.vfy(:, ti)   = d.func_vfy(d, d.xf, d.yf, d.zf, d.t(ti));
        d.vfz(:, ti)   = d.func_vfz(d, d.xf, d.yf, d.zf, d.t(ti));
        d.wfx(:, ti)   = d.func_wfx(d, d.xf, d.yf, d.zf, d.t(ti));
        d.wfy(:, ti)   = d.func_wfy(d, d.xf, d.yf, d.zf, d.t(ti));
        d.wfz(:, ti)   = d.func_wfz(d, d.xf, d.yf, d.zf, d.t(ti));
        d.pf(:,  ti)   = d.func_pf(d,  d.xf, d.yf, d.zf, d.t(ti));
        d.usx(:, ti)   = d.func_usx(d, d.xs, d.ys, d.zs, d.t(ti));
        d.usy(:, ti)   = d.func_usy(d, d.xs, d.ys, d.zs, d.t(ti));
        d.usz(:, ti)   = d.func_usz(d, d.xs, d.ys, d.zs, d.t(ti));
        d.vsx(:, ti)   = d.func_vsx(d, d.xs, d.ys, d.zs, d.t(ti));
        d.vsy(:, ti)   = d.func_vsy(d, d.xs, d.ys, d.zs, d.t(ti));
        d.vsz(:, ti)   = d.func_vsz(d, d.xs, d.ys, d.zs, d.t(ti));
        d.ps(:,  ti)   = d.func_ps(d,  d.xs, d.ys, d.zs, d.t(ti));
    end

end