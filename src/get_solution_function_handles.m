%% main function in this file that returns function handles
function d = get_solution_function_handles(d)

    d               = get_constants(d);

    if d.dynamicF
        if d.nonlinear
            d.func_pf       = @transient_navierstokes_p;
            d.func_vfx      = @transient_navierstokes_vx;
            d.func_vfy      = @transient_navierstokes_vy;
            d.func_vfz      = @transient_navierstokes_vz;
            d.func_wfx      = @transient_navierstokes_wx;
            d.func_wfy      = @transient_navierstokes_wy;
            d.func_wfz      = @transient_navierstokes_wz;
        else
            d.func_pf       = @transient_stokes_p;
            d.func_vfx      = @transient_stokes_vx;
            d.func_vfy      = @transient_stokes_vy;
            d.func_vfz      = @transient_stokes_vz;
            d.func_wfx      = @transient_stokes_wx;
            d.func_wfy      = @transient_stokes_wy;
            d.func_wfz      = @transient_stokes_wz;
        end
    else
        if d.nonlinear
            d.func_pf       = @steady_navierstokes_p;
            d.func_vfx      = @steady_navierstokes_vx;
            d.func_vfy      = @steady_navierstokes_vy;
            d.func_vfz      = @steady_navierstokes_vz;
            d.func_wfx      = @steady_navierstokes_wx;
            d.func_wfy      = @steady_navierstokes_wy;
            d.func_wfz      = @steady_navierstokes_wz;
        else
            d.func_pf       = @steady_stokes_p;
            d.func_vfx      = @steady_stokes_vx;
            d.func_vfy      = @steady_stokes_vy;
            d.func_vfz      = @steady_stokes_vz;
            d.func_wfx      = @steady_stokes_wx;
            d.func_wfy      = @steady_stokes_wy;
            d.func_wfz      = @steady_stokes_wz;
        end
    end
    d.func_vf               = {@d.func_vfx; @d.func_vfy; @d.func_vfz};
    d.func_wf               = {@d.func_wfx; @d.func_wfy; @d.func_wfz};
    if d.dynamicS
        if d.nonlinear
            d.func_ps       = @transient_neohooke_p;
            d.func_usx      = @transient_neohooke_ux;
            d.func_usy      = @transient_neohooke_uy;
            d.func_vsx      = @transient_neohooke_vx;
            d.func_vsy      = @transient_neohooke_vy;
            d.func_usz      = @transient_neohooke_uz;
            d.func_vsz      = @transient_neohooke_vz;
        else
            d.func_ps       = @transient_linelas_p;
            d.func_usx      = @transient_linelas_ux;
            d.func_usy      = @transient_linelas_uy;
            d.func_vsx      = @transient_linelas_vx;
            d.func_vsy      = @transient_linelas_vy;
            d.func_usz      = @transient_linelas_uz;
            d.func_vsz      = @transient_linelas_vz;
        end
    else
        if d.nonlinear
            d.func_ps       = @steady_neohooke_p;
            d.func_usx      = @steady_neohooke_ux;
            d.func_usy      = @steady_neohooke_uy;
            d.func_vsx      = @steady_neohooke_vx;
            d.func_vsy      = @steady_neohooke_vy;
            d.func_usz      = @steady_neohooke_uz;
            d.func_vsz      = @steady_neohooke_vz;
        else
            d.func_ps       = @steady_linelas_p;
            d.func_usx      = @steady_linelas_ux;
            d.func_usy      = @steady_linelas_uy;
            d.func_vsx      = @steady_linelas_vx;
            d.func_vsy      = @steady_linelas_vy;
            d.func_usz      = @steady_linelas_uz;
            d.func_vsz      = @steady_linelas_vz;
        end
    end
    d.func_us               = {@d.func_usx; @d.func_usy; @d.func_usz};
    d.func_vs               = {@d.func_vsx; @d.func_vsy; @d.func_vsz};

end

%% set constants
function d = get_constants(d)

    % first, set some general constants
    % see Table 1 in arxiv preprint
    d.kf            = sqrt(d.rhof * 1i * d.w / d.muf);
    d.ks            = d.w * sqrt(d.rhos / d.mus);
    d.alpha         = exp(d.kf * d.Hi) + exp(-d.kf * d.Hi);
    d.beta          = d.muf * d.kf * (exp(d.kf * d.Hi) - exp(-d.kf * d.Hi));
    d.J0sr          = besselj(0, -d.ks * d.Ho);
    d.Y0sr          = bessely(0, -d.ks * d.Ho);
    d.J0fast        = besselj(0, 1i * d.kf * d.Hi);
    d.J1fast        = d.kf * besselj(1, 1i * d.kf * d.Hi);
    d.J0sast        = besselj(0, -d.ks * d.Hi);
    d.J1sast        = 1i * d.ks * besselj(1, -d.ks * d.Hi);
    d.Y0sast        = bessely(0, -d.ks * d.Hi);
    d.Y1sast        = 1i * d.ks * bessely(1, -d.ks * d.Hi);
    d.gamma         = d.J0sr / d.Y0sr;
    d.Delta0        = d.J0sast - d.gamma * d.Y0sast;
    d.Delta1        = d.J1sast - d.gamma * d.Y1sast;
    d.nu0           = d.Y0sast / d.Y0sr;
    d.nu1           = d.Y1sast / d.Y0sr;
    d.xi1           = sin(d.ks * d.Hi) + cot(d.ks * d.Ho) * cos(d.ks * d.Hi);
    d.xi2           = cot(d.ks * d.Ho) * sin(d.ks * d.Hi) - cos(d.ks * d.Hi);
    d.zeta1         = csc(d.ks * d.Ho) * cos(d.ks * d.Hi);
    d.zeta2         = 1.0 - sin(d.ks * d.Hi) * csc(d.ks * d.Ho);

    if (d.nd == 2)
        % 2D
        if (d.dynamicF && d.dynamicS)
            %% transient fluid, transient solid
            if d.nonlinear
                d.c     = zeros(4, 1);
                d.c(1)  = 1i * d.P / (d.rhos * d.rhof * d.w * d.ks) ...
                            * (d.w^2 * d.xi2 * d.zeta1 * d.rhos * d.rhof - d.mus * d.ks^2 * d.xi1 * (d.rhos - d.zeta2 * d.rhof)) ...
                            / (1i * d.w * d.xi2 * d.beta - d.alpha * d.mus * d.ks * d.xi1);
                d.c(2)  = d.c(1);
                d.c(3)  = -d.P;
                d.c(4)  = d.P / (d.rhos * d.rhof * d.w * d.ks) ...
                            * (-d.w * d.alpha * d.zeta1 * d.rhos * d.rhof + 1i * d.beta * d.ks * (d.rhos - d.zeta2 * d.rhof)) ...
                            / (1i * d.w * d.xi2 * d.beta - d.alpha * d.mus * d.ks * d.xi1);
            else
                d.c     = zeros(4, 1);
                d.c(1)  = (d.mus * d.ks * cos(d.ks * (d.Hi - d.Ho)) * (d.P / (1i * d.rhof * d.w) ...
                            + 1i * d.P / (d.rhos * d.w)) ...
                            - 1i * d.w * d.mus * d.ks * d.P / (d.rhos * d.w^2)) ...
                            / (-d.mus * d.ks * d.alpha * cos(d.ks * (d.Hi - d.Ho)) ...
                            + 1i * d.w * d.beta * sin(d.ks * (d.Hi - d.Ho)));
                d.c(2)  = d.c(1);
                d.c(3)  = (d.beta * cos(d.ks * d.Ho) * (d.P / (1i * d.rhof * d.w) + 1i * d.P / (d.rhos * d.w)) ...
                            - (1i * d.w * d.beta * cos(d.ks * d.Hi) + d.alpha * d.mus * d.ks * sin(d.ks * d.Hi)) * d.P / (d.rhos * d.w^2)) ...
                            / (-d.mus * d.ks * d.alpha * cos(d.ks * (d.Hi - d.Ho)) ...
                            + 1i * d.w * d.beta * sin(d.ks * (d.Hi - d.Ho)));
                d.c(4)  = d.P / (d.rhos * d.w^2) * sec(d.ks * d.Ho) - tan(d.ks * d.Ho) * d.c(3);
            end
        elseif (d.dynamicF && ~d.dynamicS)
            %% transient fluid, quasi-static solid
            if d.nonlinear
                d.c     = zeros(4, 1);
                d.c(1)  = (-1i * d.w * (d.Ho - d.Hi) * d.Hi * d.P ...
                            + d.mus * (1i * d.P / (d.rhof * d.w) + 0.5 * 1i * d.w * d.P / d.mus * (d.Ho^2 - d.Hi^2))) ...
                            / (1i * d.w * d.beta * (d.Ho - d.Hi) + d.mus * d.alpha);
                d.c(2)  = d.c(1);
                d.c(3)  = -d.P;
                d.c(4)  = (d.alpha * d.Hi * d.P ...
                            + d.beta * (1i * d.P / (d.rhof * d.w) + 0.5 * 1i * d.w * d.P / d.mus * (d.Ho^2 - d.Hi^2))) ...
                            / (1i * d.w * d.beta * (d.Ho - d.Hi) + d.mus * d.alpha);
            else
                d.c     = zeros(4, 1);
                d.c(1)  = (1i * d.mus * d.P / (d.rhof * d.w) ...
                            + 0.5 * 1i * d.w * d.P * (d.Ho^2 - d.Hi^2) ...
                            - 1i * d.w * d.P * (d.Ho - d.Hi) * d.Hi) ...
                            / (d.alpha * d.mus + 1i * d.w * (d.Ho - d.Hi) * d.beta);
                d.c(2)  = d.c(1);
                d.c(3)  = ((1i * d.P / (d.rhof * d.w) ...
                            + 0.5 * 1i * d.w * d.P / d.mus * (d.Ho^2 - d.Hi^2)) * d.beta + d.P * d.Hi * d.alpha) ...
                            / (d.alpha * d.mus + 1i * d.w * (d.Ho - d.Hi) * d.beta);
                d.c(4)  = 0.5 * d.P * d.Ho^2 / d.mus ...
                            - d.Ho * ((1i * d.P / (d.rhof * d.w) ...
                            + 0.5 * 1i * d.w * d.P / d.mus * (d.Ho^2 - d.Hi^2)) * d.beta + d.P * d.Hi * d.alpha) ...
                            / (d.alpha * d.mus + 1i * d.w * (d.Ho - d.Hi) * d.beta);
            end
        elseif (~d.dynamicF && d.dynamicS)
            %% quasi-static fluid, transient solid
            if d.nonlinear
                d.c     = zeros(4, 1);
                d.c(1)  = 0.5 * d.P * d.Hi^2 / d.muf - 1i * d.zeta2 * d.P / (d.rhos * d.w) ...
                            - 1i * d.xi2 * (d.mus * d.ks * d.zeta1 * d.P + d.rhos * d.w^2 * d.P * d.Hi) / (d.mus * d.ks * d.rhos * d.w * d.xi1);
                d.c(2)  = 0;
                d.c(3)  = -d.P;
                d.c(4)  = (d.mus * d.ks * d.zeta1 * d.P + d.rhos * d.w^2 * d.P * d.Hi) ...
                            / (d.mus * d.ks * d.rhos * d.w^2 * d.xi1);
            else
                d.c     = zeros(4, 1);
                d.c(1)  = 0.5 * d.P * d.Hi^2 / d.muf ...
                            - 1i * d.P / (d.rhos * d.w) ...
                            + d.P * d.Hi * 1i * d.w / (d.mus * d.ks) * tan(d.ks * (d.Ho - d.Hi)) ...
                            + 1i * d.P / (d.rhos * d.w) * sec(d.ks * (d.Ho - d.Hi));
                d.c(3)  = (1.0 / (d.rhos * d.w^2) * sin(d.ks * d.Hi) - d.Hi / (d.mus * d.ks) * cos(d.ks * d.Ho)) ...
                            * d.P * sec(d.ks * (d.Ho - d.Hi));
                d.c(4)  = (d.Hi / (d.mus * d.ks) * sin(d.ks * d.Ho) + 1.0 / (d.rhos * d.w^2) * cos(d.ks * d.Hi)) ...
                            * d.P * sec(d.ks * (d.Ho - d.Hi));
            end
        else
            %% quasi-static fluid, quasi-static solid
            if d.nonlinear
                d.c     = zeros(4, 1);
                d.c(1)  = 0.5 * d.P * d.Hi^2 / d.muf + 0.5 * 1i * d.w * d.P / d.mus * (d.Ho^2 - d.Hi^2);
                d.c(2)  = 0.0;
                d.c(3)  = -d.P;
                d.c(4)  = 0.0;
            else
                d.c     = zeros(4, 1);
                d.c(1)  = 0.5 * d.P * d.Hi^2 / d.muf + 0.5 * 1i * d.w * d.P / d.mus * (d.Ho^2 - d.Hi^2);
                d.c(4)  = 0.5 * d.P * d.Ho^2 / d.mus;
            end
        end
    else
        % 3D
        if (d.dynamicF && d.dynamicS)
            %% transient fluid, transient solid
            if d.nonlinear
                d.c     = zeros(4, 1);
                d.c(1)  = - (1i * d.w * d.Delta0 * d.nu1 * d.P / d.ks^2 ...
                            + d.mus * d.Delta1 * ((1.0 - d.nu0) * 1i * d.w * d.P / (d.mus * d.ks^2) + d.P / (d.muf * d.kf^2))) ...
                            / (d.mus * d.J0fast * d.Delta1 - 1i * d.w * d.muf * d.J1fast * d.Delta0);
                d.c(3)  = -d.P;
                d.c(4)  = - (d.J0fast * d.nu1 * d.P / d.ks^2 + d.muf * d.J1fast * ((1.0 - d.nu0) * 1i * d.w * d.P / (d.mus * d.ks^2) + d.P / (d.muf * d.kf^2))) ...
                            / (d.mus * d.J0fast * d.Delta1 - 1i * d.w * d.muf * d.J1fast * d.Delta0);
            else
                d.c     = zeros(4, 1);
                d.c(1)  = (-d.nu1 * d.Delta0 * d.mus * d.P / (d.rhos * d.w) ...
                            - d.mus * (1.0 - d.nu0) * d.Delta1 * d.P / (d.rhos * d.w) ...
                            + 1i * d.mus * d.Delta1 * d.P / (d.muf * d.kf^2)) ...
                            / (-d.muf * d.J1fast * d.w * d.Delta0 - 1i * d.mus * d.J0fast * d.Delta1);
                d.c(3)  = (1i * d.nu1 * d.J0fast * d.mus * d.P / (d.rhos * d.w^2) ...
                            - d.muf * (1.0 - d.nu0) * d.J1fast * d.P / (d.rhos * d.w) ...
                            + 1i * d.J1fast * d.P / d.kf^2) ...
                            / (-d.muf * d.J1fast * d.w * d.Delta0 - 1i * d.mus * d.J0fast * d.Delta1);
            end
        elseif (d.dynamicF && ~d.dynamicS)
            %% transient fluid, quasi-static solid
            if d.nonlinear
                d.c     = zeros(4, 1);
                d.c(1)  = -1i * d.P * (0.5 * d.w * d.Hi^2 * log(d.Hi / d.Ho) ...
                            + 0.25 * d.w * (d.Ho^2 - d.Hi^2) ...
                            + 1i * d.mus / (d.muf * d.kf^2)) ...
                            / (d.muf * d.w * d.Hi * log(d.Hi / d.Ho) * d.J1fast - d.mus * d.J0fast);
                d.c(3)  = -d.P;
                d.c(4)  = -d.P * (0.5 * d.mus * d.J0fast * d.Hi ...
                            + 0.25 * d.muf * d.w * d.J1fast * (d.Ho^2 - d.Hi^2) ...
                            + 1i * d.mus * d.J1fast / d.kf^2) ...
                            / (d.muf * d.mus * d.w * log(d.Hi / d.Ho) * d.J1fast - d.mus^2 * d.J0fast / d.Hi);
            else
                d.c     = zeros(4, 1);
                d.c(1)  = (1i * d.w * log(d.Hi / d.Ho) * 0.5 * d.P * d.Hi ...
                            + 0.25 * 1i * d.w * d.P / d.Hi * (d.Ho^2 - d.Hi^2) - d.mus * d.P / (d.muf * d.kf^2 * d.Hi)) ...
                            / (-d.w * d.muf * d.J1fast * log(d.Hi / d.Ho) + d.J0fast * d.mus / d.Hi);
                d.c(3)  = (d.J0fast * 0.5 * d.P * d.Hi ...
                            + d.J1fast * 0.25 * d.muf * d.w * d.P / d.mus * (d.Ho^2 - d.Hi^2) ...
                            + d.J1fast * 1i * d.P / d.kf^2) ...
                            / (-d.w * d.muf * d.J1fast * log(d.Hi / d.Ho) + d.J0fast * d.mus / d.Hi);
            end
        elseif (~d.dynamicF && d.dynamicS)
            %% quasi-static fluid, transient solid
            if d.nonlinear
                d.c     = zeros(4, 1);
                d.c(1)  = 0.25 * d.P * d.Hi^2 / d.muf ...
                            - 1i * d.w * d.P / (d.mus * d.ks^2) * (1.0 - d.nu0) ...
                            - d.P * d.w * (2.0 * 1i * d.Y1sast - d.Hi * d.ks^2 * d.Y0sr) ...
                            / (2.0 * d.mus * d.ks^2 * d.Y0sr) ...
                            * d.Delta0 / d.Delta1;
                d.c(3)  = -d.P;
                d.c(4)  = -d.P * (2.0 * d.Y1sast + 1i * d.Hi * d.ks^2 * d.Y0sr) ...
                            / (2.0 * d.mus * d.ks^2 * d.Y0sr * d.Delta1);
            else
                d.c     = zeros(4, 1);
                d.c(1)  = 0.25 * d.P * d.Hi^2 / d.muf ...
                            - 1i * d.w * d.P ...
                            * ((1.0 - d.nu0) / (d.rhos * d.w^2) ...
                            + 1i * d.Delta0 / d.Delta1 * (0.5 * d.Hi / d.mus - 1i * d.nu1 / (d.rhos * d.w^2)));
                d.c(3)  = d.P / (1i * d.Delta1) * (0.5 * d.Hi / d.mus - 1i * d.nu1 / (d.rhos * d.w^2));
            end
        else
            %% quasi-static fluid, quasi-static solid
            if d.nonlinear
                d.c     = zeros(4, 1);
                d.c(1)  = 0.25 * d.P * d.Hi^2 / d.muf + 0.25 * 1i * d.w * d.P * (d.Ho^2 - d.Hi^2) / d.mus;
                d.c(3)  = -d.P;
            else
                d.c     = zeros(4, 1);
                d.c(1)  = 0.25 * d.P * d.Hi^2 / d.muf + 0.25 * 1i * d.w * d.P * (d.Ho^2 - d.Hi^2) / d.mus;
                d.c(2)  = 0.0;
                d.c(3)  = 0.0;
                d.c(4)  = 0.0;
            end
        end
    end

end

%% transient Stokes equation
% fluid pressure
function val = transient_stokes_p(d, x, y, z, t)
    if (d.nd == 2)
        val = real(d.Pf * (d.L - x) .* exp(1i * d.w * t));
    else
        val = real(d.Pf * (d.L - z) .* exp(1i * d.w * t));
    end
end

% fluid x-velocity
function val = transient_stokes_vx(d, x, y, z, t)
    if (d.nd == 2)
        val = real((- 1i * d.Pf / (d.rhof * d.w) + d.c(1) * exp(d.kf * y) + d.c(2) * exp(-d.kf * y)) ...
                .* exp(1i * d.w * t));
    else
        val = 0.0 .* x;
    end
end

% fluid y-velocity
function val = transient_stokes_vy(d, x, y, z, t)
    val = 0.0 .* y;
end

% fluid z-velocity
function val = transient_stokes_vz(d, x, y, z, t)
    if (d.nd == 2)
        val = 0.0 .* z;
    else
        r       = sqrt(x.^2 + y.^2);
        val = real((d.P / (d.muf * d.kf^2) + d.c(1) * besselj(0, 1i * d.kf * r) ...
                    + d.c(2) * bessely(0, 1i * d.kf * r)) .* exp(1i * d.w * t));
    end
end

% fluid domain x-velocity
function val = transient_stokes_wx(d, x, y, z, t)
    val = 0.0 .* x;
end

% fluid domain y-velocity
function val = transient_stokes_wy(d, x, y, z, t)
    val = 0.0 .* y;
end

% fluid domain z-velocity
function val = transient_stokes_wz(d, x, y, z, t)
    val = 0.0 .* z;
end

%% transient Navier-Stokes equation
% fluid pressure
function val = transient_navierstokes_p(d, x, y, z, t)
    if (d.nd == 2)
        val = real(d.Pf * (d.L - x) .* exp(1i * d.w * t));
    else
        val = real(d.Pf * (d.L - z) .* exp(1i * d.w * t));
    end
end

% fluid x-velocity
function val = transient_navierstokes_vx(d, x, y, z, t)
    if (d.nd == 2)
        val = real((- 1i * d.Pf / (d.rhof * d.w) + d.c(1) * exp(d.kf * y) + d.c(2) * exp(-d.kf * y)) ...
                .* exp(1i * d.w * t));
    else
        val = 0.0 .* x;
    end
end

% fluid y-velocity
function val = transient_navierstokes_vy(d, x, y, z, t)
    val = 0.0 .* y;
end

% fluid z-velocity
function val = transient_navierstokes_vz(d, x, y, z, t)
    if (d.nd == 2)
        val = 0.0 .* z;
    else
        r   = sqrt(x.^2 + y.^2);
        val = real((d.P / (d.muf * d.kf^2) + d.c(1) * besselj(0, 1i * d.kf * r) ...
                + d.c(2) * bessely(0, 1i * d.kf * r)) .* exp(1i * d.w * t));
    end
end

% fluid domain x-velocity
function val = transient_navierstokes_wx(d, x, y, z, t)
    if (d.nd == 2)
        val = real((- 1i * d.Pf / (d.rhof * d.w) ...
                + d.c(1) * exp(d.kf * (y .* 0.0 + d.Hi)) + d.c(2) * exp(-d.kf * (y .* 0.0 + d.Hi))) ...
                .* exp(1i * d.w * t));
    else
        val = 0.0 .* x;
    end
end

% fluid domain y-velocity
function val = transient_navierstokes_wy(d, x, y, z, t)
    val = 0.0 .* y;
end

% fluid domain z-velocity
function val = transient_navierstokes_wz(d, x, y, z, t)
    if (d.nd == 2)
        val = 0.0 .* z;
    else
        r   = sqrt(x.^2 + y.^2);
        val = real((d.P / (d.muf * d.kf^2) + d.c(1) * besselj(0, 1i * d.kf * (r .* 0.0 + d.Hi)) ...
                + d.c(2) * bessely(0, 1i * d.kf * (r .* 0.0 + d.Hi))) .* exp(1i * d.w * t));
    end
end

%% quasi-static Stokes equation
% fluid pressure
function val = steady_stokes_p(d, x, y, z, t)
    if (d.nd == 2)
        val = real(d.Pf * (d.L - x) .* exp(1i * d.w * t));
    else
        val = real(d.Pf * (d.L - z) .* exp(1i * d.w * t));
    end
end

% fluid x-velocity
function val = steady_stokes_vx(d, x, y, z, t)
    if (d.nd == 2)
        val = real((- d.Pf / (2.0 * d.muf) * y.^2 + d.c(2) .* y + d.c(1)) .* exp(1i * d.w * t));
    else
        val = 0.0 .* x;
    end
end

% fluid y-velocity
function val = steady_stokes_vy(d, x, y, z, t)
    val = 0.0 .* y;
end

% fluid z-velocity
function val = steady_stokes_vz(d, x, y, z, t)
    if (d.nd == 2)
        val = 0.0 .* z;
    else
        r   = sqrt(x.^2 + y.^2);
        val = real((-0.25 * d.P * r.^2 / d.muf + d.c(1)) .* exp(1i * d.w * t));
    end
end

% fluid domain x-velocity
function val = steady_stokes_wx(d, x, y, z, t)
    val = 0.0 .* x;
end

% fluid domain y-velocity
function val = steady_stokes_wy(d, x, y, z, t)
    val = 0.0 .* y;
end

% fluid domain z-velocity
function val = steady_stokes_wz(d, x, y, z, t)
    val = 0.0 .* z;
end

%% quasi-static Navier-Stokes equation
% fluid pressure
function val = steady_navierstokes_p(d, x, y, z, t)
    if (d.nd == 2)
        val = real(d.Pf * (d.L - x) .* exp(1i * d.w * t));
    else
        val = real(d.Pf * (d.L - z) .* exp(1i * d.w * t));
    end
end

% fluid x-velocity
function val = steady_navierstokes_vx(d, x, y, z, t)
    if (d.nd == 2)
        val = real((- d.Pf / (2.0 * d.muf) * y.^2 + d.c(2) .* y + d.c(1)) .* exp(1i * d.w * t));
    else
        val = 0.0 .* x;
    end
end

% fluid y-velocity
function val = steady_navierstokes_vy(d, x, y, z, t)
    val = 0.0 .* y;
end

% fluid z-velocity
function val = steady_navierstokes_vz(d, x, y, z, t)
    if (d.nd == 2)
        val = 0.0 .* z;
    else
        r   = sqrt(x.^2 + y.^2);
        val = real((-0.25 * d.P * r.^2 / d.muf + d.c(1)) .* exp(1i * d.w * t));
    end
end

% fluid domain x-velocity
function val = steady_navierstokes_wx(d, x, y, z, t)
    if (d.nd == 2)
        val = real((- d.Pf / (2.0 * d.muf) * (y .* 0.0 + d.Hi).^2 ...
                + d.c(2) .* (y .* 0.0 + d.Hi) ...
                + d.c(1)) .* exp(1i * d.w * t));
    else
        val = 0.0 .* x;
    end
end

% fluid domain y-velocity
function val = steady_navierstokes_wy(d, x, y, z, t)
    val = 0.0 .* y;
end

% fluid domain z-velocity
function val = steady_navierstokes_wz(d, x, y, z, t)
    if (d.nd == 2)
        val = 0.0 .* z;
    else
        r   = sqrt(x.^2 + y.^2);
        val = real((-0.25 * d.P * (r .* 0.0 + d.Hi).^2 / d.muf ...
                + d.c(1)) .* exp(1i * d.w * t));
    end
end

%% transient linear-elastic solid
% solid pressure
function val = transient_linelas_p(d, x, y, z, t)
    if (d.nd == 2)
        val = real(d.Ps * (d.L - x) .* exp(1i * d.w * t));
    else
        val = real(d.Ps * (d.L - z) .* exp(1i * d.w * t));
    end
end

% solid x-displacement
function val = transient_linelas_ux(d, x, y, z, t)
    if (d.nd == 2)
        val = real((-d.Ps / (d.rhos * d.w^2) + d.c(3) * sin(d.ks * y) + d.c(4) * cos(d.ks * y)) .* exp(1i * d.w * t));
    else
        val = 0.0 .* y;
    end
end

% solid y-displacement
function val = transient_linelas_uy(d, x, y, z, t)
    val = 0.0 .* y;
end

% solid z-displacement
function val = transient_linelas_uz(d, x, y, z, t)
    if (d.nd == 2)
        val = 0.0 .* z;
    else
        r   = sqrt(x.^2 + y.^2);
        val = real((-d.Ps / (d.rhos * d.w^2) * (1.0 - bessely(0, -d.ks * r) / d.Y0sr) ...
                + d.c(3) * (besselj(0, -d.ks * r) - d.gamma * bessely(0, -d.ks * r))) .* exp(1i * d.w * t));
    end
end

% solid x-velocity
function val = transient_linelas_vx(d, x, y, z, t)
    if (d.nd == 2)
        val = real(1i * d.w * (-d.Ps / (d.rhos * d.w^2) + d.c(3) * sin(d.ks * y) + d.c(4) * cos(d.ks * y)) .* exp(1i * d.w * t));
    else
        val = 0.0 .* y;
    end
end

% solid y-velocity
function val = transient_linelas_vy(d, x, y, z, t)
    val = 0.0 .* y;
end

% solid z-velocity
function val = transient_linelas_vz(d, x, y, z, t)
    if (d.nd == 2)
        val = 0.0 .* z;
    else
        r   = sqrt(x.^2 + y.^2);
        val = real(1i * d.w * (-d.Ps / (d.rhos * d.w^2) * (1.0 - bessely(0, -d.ks * r) / d.Y0sr) ...
                + d.c(3) * (besselj(0, -d.ks * r) - d.gamma * bessely(0, -d.ks * r))) .* exp(1i * d.w * t));
    end
end

%% transient Neo-Hooke solid
% solid pressure
function val = transient_neohooke_p(d, x, y, z, t)
    if (d.nd == 2)
        val = (d.L - x + real(d.P / (d.rhos * d.w^2) * (1.0 - sin(d.ks * y) * csc(d.ks * d.Ho) - d.zeta2) .* exp(1i * d.w * t))) ...
                .* real(d.P * exp(1i * d.w * t)) ...
                - real(d.c(4) * (cos(d.ks * y) - cot(d.ks * d.Ho) * sin(d.ks * y) + d.xi2) .* exp(1i * d.w * t)) ...
                .* real(d.P * exp(1i * d.w * t)) ...
                - d.mus / 2.0 * (real(d.P * d.ks / (d.rhos * d.w^2) * cos(d.ks * y) * csc(d.ks * d.Ho) .* exp(1i * d.w * t) ...
                - d.c(4) * d.ks * (sin(d.ks * y) + cot(d.ks * d.Ho) * cos(d.ks * y)) .* exp(1i * d.w * t))).^2.0;
    else
        r   = sqrt(x.^2 + y.^2);
        val = (d.L - z - real(d.P / (d.mus * d.ks^2) * (bessely(0, -d.ks * r) / d.Y0sr - d.nu0) .* exp(1i * d.w * t))) ...
                .* real(d.P * exp(1i * d.w * t)) ...
                + real(d.c(4) * (d.Delta0 - besselj(0, -d.ks * r) + d.gamma * bessely(0, -d.ks * r)) .* exp(1i * d.w * t)) ...
                .* real(d.P * exp(1i * d.w * t)) ...
                - d.mus / 3.0 * (real(d.P / (d.mus * d.ks) * bessely(1, -d.ks * r) / d.Y0sr .* exp(1i * d.w * t) ...
                + d.c(4) * d.ks * (besselj(1, -d.ks * r) - d.gamma * bessely(1, -d.ks * r)) .* exp(1i * d.w * t))).^2;
    end
end

% solid x-displacement
function val = transient_neohooke_ux(d, x, y, z, t)
    if (d.nd == 2)
        val = real((d.c(3) / (d.rhos * d.w^2) * (1.0 - sin(d.ks * y) * csc(d.ks * d.Ho)) ...
                + d.c(4) * (cos(d.ks * y) - cot(d.ks * d.Ho) * sin(d.ks * y))) .* exp(1i * d.w * t));
    else
        val = 0.0 .* x;
    end
end

% solid y-displacement
function val = transient_neohooke_uy(d, x, y, z, t)
    val = 0.0 .* y;
end

% solid z-displacement
function val = transient_neohooke_uz(d, x, y, z, t)
    if (d.nd == 2)
        val = 0.0 .* z;
    else
        r   = sqrt(x.^2 + y.^2);
        val = real((d.c(3) / (d.mus * d.ks^2) * (1.0 - bessely(0, -d.ks * r) / d.Y0sr) ...
                + d.c(4) * (besselj(0, -d.ks * r) - d.gamma * bessely(0, -d.ks * r))) .* exp(1i * d.w * t));
    end
end

% solid x-velocity
function val = transient_neohooke_vx(d, x, y, z, t)
    if (d.nd == 2)
        val = real(1i * d.w * (d.c(3) / (d.rhos * d.w^2) * (1.0 - sin(d.ks * y) * csc(d.ks * d.Ho)) ...
                + d.c(4) * (cos(d.ks * y) - cot(d.ks * d.Ho) * sin(d.ks * y))) .* exp(1i * d.w * t));
    else
        val = 0.0 .* y;
    end
end

% solid y-velocity
function val = transient_neohooke_vy(d, x, y, z, t)
    val = 0.0 .* y;
end

% solid z-velocity
function val = transient_neohooke_vz(d, x, y, z, t)
    if (d.nd == 2)
        val = 0.0 .* z;
    else
        r   = sqrt(x.^2 + y.^2);
        val = real(1i * d.w * (d.c(3) / (d.mus * d.ks^2) * (1.0 - bessely(0, -d.ks * r) / d.Y0sr) ...
                + d.c(4) * (besselj(0, -d.ks * r) - d.gamma * bessely(0, -d.ks * r))) .* exp(1i * d.w * t));
    end
end

%% quasi-static linear-elastic solid
% solid pressure
function val = steady_linelas_p(d, x, y, z, t)
    if (d.nd == 2)
        val = real(d.Ps * (d.L - x) .* exp(1i * d.w * t));
    else
        val = real(d.Ps * (d.L - z) .* exp(1i * d.w * t));
    end
end

% solid x-displacement
function val = steady_linelas_ux(d, x, y, z, t)
    if (d.nd == 2)
        val = real((- d.Ps / (2.0 * d.mus) * y.^2 + d.c(3) * y + d.c(4)) .* exp(1i * d.w * t));
    else
        val = 0.0 .* y;
    end
end

% solid y-displacement
function val = steady_linelas_uy(d, x, y, z, t)
    val = 0.0 .* y;
end

% solid z-displacement
function val = steady_linelas_uz(d, x, y, z, t)
    if (d.nd == 2)
        val = 0.0 .* z;
    else
        r   = sqrt(x.^2 + y.^2);
        val = real((0.25 * d.Ps / d.mus * (d.Ho^2 - r.^2) + d.c(3) * log(r / d.Ho)) .* exp(1i * d.w * t));
    end
end

% solid x-velocity
function val = steady_linelas_vx(d, x, y, z, t)
    if (d.nd == 2)
        val = real(1i * d.w * (- d.Ps / (2.0 * d.mus) * y.^2 + d.c(3) * y + d.c(4)) .* exp(1i * d.w * t));
    else
        val = 0.0 .* y;
    end
end

% solid y-velocity
function val = steady_linelas_vy(d, x, y, z, t)
    val = 0.0 .* y;
end

% solid z-velocity
function val = steady_linelas_vz(d, x, y, z, t)
    if (d.nd == 2)
        val = 0.0 .* z;
    else
        r   = sqrt(x.^2 + y.^2);
        val = real(1i * d.w * (0.25 * d.Ps / d.mus * (d.Ho^2 - r.^2) + d.c(3) * log(r / d.Ho)) .* exp(1i * d.w * t));
    end
end

%% quasi-static Neo-Hooke solid
% solid pressure
function val = steady_neohooke_p(d, x, y, z, t)
    if (d.nd == 2)
        val = (d.L - x + real(0.5 * d.P / d.mus * (y.^2 - d.Hi^2) .* exp(1i * d.w * t) ...
                + d.c(4) * (d.Hi - y) .* exp(1i * d.w * t))) ...
                .* real(d.P * exp(1i * d.w * t)) ...
                - 0.5 * d.mus * real(d.c(4) * exp(1i * d.w * t) - d.P * y / d.mus .* exp(1i * d.w * t)).^2;
    else
        r   = sqrt(x.^2 + y.^2);
        val = (d.L - z + real(d.c(4) * exp(1i * d.w * t) .* log(d.Hi ./ r)) ...
                + real(0.25 * d.P / d.mus * (r.^2 - d.Hi^2) .* exp(1i * d.w * t))) ...
                .* real(d.P * exp(1i * d.w * t)) ...
                - d.mus / 3.0 * (real(d.c(4) * exp(1i * d.w * t) ./ r - 0.5 * d.P * r .* exp(1i * d.w * t) / d.mus)).^2.0;
    end
end

% solid x-displacement
function val = steady_neohooke_ux(d, x, y, z, t)
    if (d.nd == 2)
        val = real((d.c(3) / (2.0 * d.mus) * (y.^2 - d.Ho^2) + d.c(4) * (y - d.Ho)) ...
                .* exp(1i * d.w * t));
    else
        val = 0.0 .* x;
    end
end

% solid y-displacement
function val = steady_neohooke_uy(d, x, y, z, t)
    val = 0.0 .* y;
end

% solid z-displacement
function val = steady_neohooke_uz(d, x, y, z, t)
    if (d.nd == 2)
        val = 0.0 .* z;
    else
        r   = sqrt(x.^2 + y.^2);
        val = real((0.25 * d.c(3) / d.mus * (r.^2 - d.Ho^2) + d.c(4) * log(r / d.Ho)) .* exp(1i * d.w * t));
    end
end

% solid x-velocity
function val = steady_neohooke_vx(d, x, y, z, t)
    if (d.nd == 2)
        val = real((d.c(3) / (2.0 * d.mus) * (y.^2 - d.Ho^2) + d.c(4) * (y - d.Ho)) ...
                * 1i * d.w .* exp(1i * d.w * t));
    else
        val = 0.0 .* x;
    end
end

% solid y-velocity
function val = steady_neohooke_vy(d, x, y, z, t)
    val = 0.0 .* y;
end

% solid z-velocity
function val = steady_neohooke_vz(d, x, y, z, t)
    if (d.nd == 2)
        val = 0.0 .* z;
    else
        r   = sqrt(x.^2 + y.^2);
        val = real(1i * d.w * (0.25 * d.c(3) / d.mus * (r.^2 - d.Ho^2) + d.c(4) * log(r / d.Ho)) .* exp(1i * d.w * t));
    end
end
