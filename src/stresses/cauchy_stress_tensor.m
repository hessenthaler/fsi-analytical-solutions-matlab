function C = cauchy_stress_tensor(d, func, funcp, mu, x, t)
    nd          = d.nd;
    id          = eye(nd, nd);
    % evaluate pressure
    if (nd == 3)
        p           = funcp(d, x(1), x(2), x(3), t);
    else
        p           = funcp(d, x(1), x(2), 0.0,  t);
    end
    % Cauchy
    C           = mu * (grad(d, func, x, t, false) + grad(d, func, x, t, false)') - p * id;
end