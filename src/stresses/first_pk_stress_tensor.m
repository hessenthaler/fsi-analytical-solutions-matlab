function P = first_pk_stress_tensor(d, func, funcp, x, t)
    nd          = d.nd;
    id          = eye(nd, nd);
    % deformation gradient
    F           = @(x, t) id + grad(d, func, x, t, false);
    % inverse of deformation gradient
    invF        = @(x, t) inv(F(x, t));
    % inverse transpose of deformation gradient
    invFT       = @(x, t) invF(x, t)';
    % evaluate pressure
    if (nd == 3)
        ps          = funcp(d, x(1), x(2), x(3), t);
    else
        ps          = funcp(d, x(1), x(2), 0.0,  t);
    end
    % first Piola-Kirchhoff
    P           = d.mus * (F(x, t) - double_dot(F(x, t), F(x, t)) / double(nd) * invFT(x, t) * invFT(x, t)) - ps * invFT(x, t);
end