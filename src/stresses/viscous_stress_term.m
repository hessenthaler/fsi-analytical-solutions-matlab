function result = viscous_stress_term(d, func, x, t)
    eps     = 1e-6;
    nd      = d.nd;
    id      = eye(nd);
    result  = zeros(nd, 1);
    for i = 1:nd
        Mp              = evaluate_cell_handles(d, func, x+eps*id(:,i), t);
        M               = evaluate_cell_handles(d, func, x,             t);
        Mm              = evaluate_cell_handles(d, func, x-eps*id(:,i), t);
        result(:)       = result(:) + (Mp - 2.0 * M + Mm) / (eps * eps);
    end
end