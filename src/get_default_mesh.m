function d = get_default_mesh(d)

    if (d.nd == 2)
        % set up mesh spacing
        dxf             = d.L / 10.0;
        dxs             = d.L / 10.0;
        dyf             = d.Hi / 100.0;
        dys             = (d.Ho - d.Hi) / 20.0;
        dt              = d.T / double(d.nt);
        xf              = 0.0:dxf:d.L;
        yf              = 0.0:dyf:d.Hi;
        xs              = 0.0:dxs:d.L;
        ys              = d.Hi:dys:d.Ho;

        % get space-time grid
        [d.Xf, d.Yf]    = meshgrid(xf, yf);
        [d.Xs, d.Ys]    = meshgrid(xs, ys);
        d.trif          = delaunay(d.Xf, d.Yf);
        d.tris          = delaunay(d.Xs, d.Ys);
        d.Zf            = 0.0 * d.Xf;
        d.Zs            = 0.0 * d.Xs;
        d.xf            = d.Xf(:);
        d.xs            = d.Xs(:);
        d.yf            = d.Yf(:);
        d.ys            = d.Ys(:);
        d.zf            = d.Zf(:);
        d.zs            = d.Zs(:);
        d.t             = 0.0:dt:d.T;
    else
        % set up mesh spacing
        dzf             = d.L / 10.0;
        dzs             = d.L / 10.0;
        dyf             = d.Hi / 100.0;
        dys             = (d.Ho - d.Hi) / 20.0;
        dt              = d.T / double(d.nt);
        zf              = 0.0:dzf:d.L;
        yf              = 0.0:dyf:d.Hi;
        zs              = 0.0:dzs:d.L;
        ys              = d.Hi:dys:d.Ho;

        % get space-time grid
        [d.Zf, d.Yf]    = meshgrid(zf, yf);
        [d.Zs, d.Ys]    = meshgrid(zs, ys);
        d.trif          = delaunay(d.Zf, d.Yf);
        d.tris          = delaunay(d.Zs, d.Ys);
        d.Xf            = 0.0 * d.Zf;
        d.Xs            = 0.0 * d.Zs;
        d.xf            = d.Xf(:);
        d.xs            = d.Xs(:);
        d.yf            = d.Yf(:);
        d.ys            = d.Ys(:);
        d.zf            = d.Zf(:);
        d.zs            = d.Zs(:);
        d.t             = 0.0:dt:d.T;
    end
    d.xyzF              = [d.xf, d.yf, d.zf];
    d.xyzS              = [d.xs, d.ys, d.zs];

end