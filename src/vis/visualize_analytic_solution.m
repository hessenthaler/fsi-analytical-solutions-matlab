function d = visualize_analytic_solution(d)

    % get data range
    d.minvfx    = min(d.vfx(:));
    d.maxvfx    = max(d.vfx(:));
    d.minvfy    = min(d.vfy(:));
    d.maxvfy    = max(d.vfy(:));
    d.minvfz    = min(d.vfz(:));
    d.maxvfz    = max(d.vfz(:));
    d.minwfx    = min(d.wfx(:));
    d.maxwfx    = max(d.wfx(:));
    d.minwfy    = min(d.wfy(:));
    d.maxwfy    = max(d.wfy(:));
    d.minwfz    = min(d.wfz(:));
    d.maxwfz    = max(d.wfz(:));
    d.minusx    = min(d.usx(:));
    d.maxusx    = max(d.usx(:));
    d.minusy    = min(d.usy(:));
    d.maxusy    = max(d.usy(:));
    d.minusz    = min(d.usz(:));
    d.maxusz    = max(d.usz(:));
    d.minvsx    = min(d.vsx(:));
    d.maxvsx    = max(d.vsx(:));
    d.minvsy    = min(d.vsy(:));
    d.maxvsy    = max(d.vsy(:));
    d.minvsz    = min(d.vsz(:));
    d.maxvsz    = max(d.vsz(:));
    d.minvx     = min(min(d.minvfx), min(d.minvsx));
    d.minvz     = min(min(d.minvfz), min(d.minvsz));
    d.maxvx     = max(max(d.maxvfx), max(d.maxvsx));
    d.maxvz     = max(max(d.maxvfz), max(d.maxvsz));
    d.minpf     = min(d.pf(:));
    d.maxpf     = max(d.pf(:));
    d.minps     = min(d.ps(:));
    d.maxps     = max(d.ps(:));
    d.minp      = min(d.minpf, d.minps);
    d.maxp      = max(d.maxpf, d.maxps);
    if (d.nd == 2)
        maskInF     = (d.xf == 0.0);
        maskOutF    = (d.xf == d.L);
        maskInS     = (d.xs == 0.0);
        maskOutS    = (d.xs == d.L);
    else
        maskInF     = (d.zf == 0.0);
        maskOutF    = (d.zf == d.L);
        maskInS     = (d.zs == 0.0);
        maskOutS    = (d.zs == d.L);
    end
    d.minpfin   = min(min(d.pf(maskInF, :)));
    d.maxpfin   = max(max(d.pf(maskInF, :)));
    d.minpsin   = min(min(d.ps(maskInS, :)));
    d.maxpsin   = max(max(d.ps(maskInS, :)));
    d.minpfout  = min(min(d.pf(maskOutF, :)));
    d.maxpfout  = max(max(d.pf(maskOutF, :)));
    d.minpsout  = min(min(d.ps(maskOutS, :)));
    d.maxpsout  = max(max(d.ps(maskOutS, :)));
    d.minpin    = min(d.minpfin, d.minpsin);
    d.maxpin    = max(d.maxpfin, d.maxpsin);
    d.minpout   = min(d.minpfout, d.minpsout);
    d.maxpout   = max(d.maxpfout, d.maxpsout);
    if ((abs(d.minpout) < 1.0e-5) && (abs(d.maxpout) < 1.0e-5))
        d.minpout   = -1.0e-5;
        d.maxpout   =  1.0e-5;
    end

    % create figure with user-defined viewport
    figure('units', 'normalized', 'outerposition', d.viewport);
    % set figure background to white
    set(gcf, 'Color', 'white');
    % default colormap
    colormap(d.colormap);
    % export frames to video object
    if d.movie_export
        d   = get_movie_fname(d);
        d.vidObj  = VideoWriter(d.movie_fname, 'MPEG-4');
        set(d.vidObj, 'FrameRate', 5);
        open(d.vidObj);
    end
    % visualize multiple time steps and render figure after adding each
    % time step
    for ti = 1:length(d.t)
        % plot velocity along y
        subplot(2, 2, 1);
        if (d.nd == 2)
            plot(d.vfx(maskInF, ti), d.yf(maskInF), 'k-');
            hold on;
            plot(d.vsx(maskInS, ti), d.ys(maskInS), 'k-.');
            plot([2.0*d.minvx, 2.0*d.maxvx], [d.Hi, d.Hi], 'r-');
            xlabel('$v~(X = 0, Y, t)$');
            axis([1.1*d.minvx, 1.1*d.maxvx, 0, d.Ho]);
        else
            plot(d.vfz(maskInF, ti), d.yf(maskInF), 'k-');
            hold on;
            plot(d.vsz(maskInS, ti), d.ys(maskInS), 'k-.');
            plot([2.0*d.minvz, 2.0*d.maxvz], [d.Hi, d.Hi], 'r-');
            xlabel('$v~(X = 0, Y, Z = 0, t)$');
            axis([1.1*d.minvz, 1.1*d.maxvz, 0, d.Ho]);
        end
        title(sprintf('velocity, $t = %.3f$', d.t(ti)));
        ylabel('$y$');
        if ~d.manyt
            hold off;
        end
        % plot displacement along y
        subplot(2, 2, 2);
        if (d.nd == 2)
            plot(real(d.usx(maskInS, ti)), d.ys(maskInS), 'k-.');
            hold on;
            plot([2.0*d.minusx, 2.0*d.maxusx], [d.Hi, d.Hi], 'r-');
            xlabel('$u_s~(X = 0, Y, t)$');
            axis([1.1*d.minusx, 1.1*d.maxusx, d.Hi, d.Ho]);
        else
            plot(real(d.usz(maskInS, ti)), d.ys(maskInS), 'k-.');
            hold on;
            plot([2.0*d.minusz, 2.0*d.maxusz], [d.Hi, d.Hi], 'r-');
            xlabel('$u_s~(X = 0, Y, Z = 0, t)$');
            axis([1.1*d.minusz, 1.1*d.maxusz, d.Hi, d.Ho]);
        end
        title(sprintf('displacement, $t = %.3f$', d.t(ti)));
        ylabel('$y$');
        if ~d.manyt
            hold off;
        end
        % plot pressure at inlet
        subplot(2, 2, 3);
        plot(real(d.pf(maskInF, ti)), d.yf(maskInF), 'k-');
        hold on;
        plot(real(d.ps(maskInS, ti)), d.ys(maskInS), 'k-.');
        plot([2.0*d.minpin, 2.0*d.maxpin], [d.Hi, d.Hi], 'r-');
        if (d.nd == 2)
            xlabel('$p~(X = 0, Y, t)$');
        else
            xlabel('$p~(X = 0, Y, Z = 0, t)$');
        end
        title(sprintf('pressure at inlet, $t = %.3f$', d.t(ti)));
        ylabel('$y$');
        axis([1.1*d.minpin, 1.1*d.maxpin, 0, d.Ho]);
        if ~d.manyt
            hold off;
        end
        % plot pressure at outlet
        subplot(2, 2, 4);
        plot(real(d.pf(maskOutF, ti)), d.yf(maskOutF), 'k-');
        hold on;
        plot(real(d.ps(maskOutS, ti)), d.ys(maskOutS), 'k-.');
        plot([2.0*d.minpout, 2.0*d.maxpout], [d.Hi, d.Hi], 'r-');
        if (d.nd == 2)
            xlabel('$p~(X = 0, Y, t)$');
        else
            xlabel('$p~(X = 0, Y, Z = L, t)$');
        end
        title(sprintf('pressure at outlet, $t = %.3f$', d.t(ti)));
        ylabel('$y$');
        axis([1.1*d.minpout, 1.1*d.maxpout, 0, d.Ho]);
        if ~d.manyt
            hold off;
        end
        % draw figure at runtime
        drawnow;
        % save current frame
        if d.movie_export
            currFrame   = getframe(gcf);
            writeVideo(d.vidObj, currFrame);
        else
            pause(0.5);
        end
    end
    % close movie object
    if d.movie_export
        close(d.vidObj);
    end
end

% use a file name that identifies the analytic solution
function d = get_movie_fname(d)
    if d.nonlinear
        fname   = 'movies/nonlinear_';
    else
        fname   = 'movies/linear_';
    end
    if (d.nd == 2)
        fname   = [fname, '2D_'];
    else
        fname   = [fname, '3D_'];
    end
    if d.dynamicF
        fname   = [fname, 'tf_'];
    else
        fname   = [fname, 'sf_'];
    end
    if d.dynamicS
        fname   = [fname, 'ts.mp4'];
    else
        fname   = [fname, 'ss.mp4'];
    end
    d.movie_fname = fname;
    fprintf('\n');
    fprintf(sprintf('Exporting movie: %s\n', d.movie_fname));
    fprintf('\n');
end