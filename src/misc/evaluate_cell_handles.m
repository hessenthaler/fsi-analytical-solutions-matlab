function result = evaluate_cell_handles(d, func, X, t)
    if (d.nd == 3)
        x       = X(1);
        y       = X(2);
        z       = X(3);
        result  = [func{1}(d, x, y, z, t); func{2}(d, x, y, z, t); func{3}(d, x, y, z, t)];
    else
        x       = X(1);
        y       = X(2);
        z       = 0.0;
        result  = [func{1}(d, x, y, z, t); func{2}(d, x, y, z, t)];
    end
end