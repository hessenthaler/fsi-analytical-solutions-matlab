function d = edit_parameters(d, str, param)

    % check whether the parameters were intitialized
    if ~d.initialized
        error('Parameter structure needs to be initialized first.');
    end

    % change parameter
    if strcmpi(str, 'number of dimensions')
        d.nd            = int32(param);
    elseif strcmpi(str, 'user mesh')
        d.userMesh      = logical(param);
    elseif strcmpi(str, 'visualize multiple time steps')
        d.manyt         = logical(param);
    elseif strcmpi(str, 'export movie')
        d.movie_export  = param;
    elseif strcmpi(str, 'export to folder')
        d.exportFolder  = param;
    elseif strcmpi(str, 'final time')
        d.T             = double(param);
        d.w             = 2.0 * pi / d.T;
    elseif strcmpi(str, 'fluid density')
        d.rhof          = double(param);
    elseif strcmpi(str, 'solid density')
        d.rhos          = double(param);
    elseif strcmpi(str, 'pressure amplitude')
        d.Pf            = double(param);
        d.Ps            = double(param);
        d.P             = double(param);
    elseif strcmpi(str, 'fluid viscosity')
        d.muf           = double(param);
    elseif strcmpi(str, 'solid stiffness')
        d.mus           = double(param);
    elseif strcmpi(str, 'inner radius')
        d.Hi            = double(param);
    elseif strcmpi(str, 'outer radius')
        d.Ho            = double(param);
    elseif strcmpi(str, 'number of time steps')
        d.nt            = int32(param);
    elseif strcmpi(str, 'length')
        d.L             = double(param);
    elseif strcmpi(str, 'dynamic fluid')
        d.dynamicF      = logical(param);
    elseif strcmpi(str, 'dynamic solid')
        d.dynamicS      = logical(param);
    elseif strcmpi(str, 'nonlinear')
        d.nonlinear     = logical(param);
    elseif strcmpi(str, 'check balance equations')
        d.balanceEqn    = logical(param);
    elseif strcmpi(str, 'number of samples (balance eqn.)')
        d.nSamples      = int32(param);
    elseif strcmpi(str, 'tolerance (balance eqn. - fluid)')
        d.balanceFtol   = double(param);
    elseif strcmpi(str, 'tolerance (balance eqn. - solid)')
        d.balanceStol   = double(param);
    elseif strcmpi(str, 'tolerance (traction constr.)');
        d.constrTtol    = double(param);
    elseif strcmpi(str, 'tolerance (velocity constr.)');
        d.constrVtol    = double(param);
    else
        error('Unknown parameter: %s', str);
    end

end