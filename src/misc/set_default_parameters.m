function d = set_default_parameters()

    d               = [];
    d.userMesh      = false;
    d.exportFolder  = 'data/';

    % pressure amplitude
    d.Pf            = 1.0;
    d.Ps            = d.Pf;
    % density
    d.rhof          = 1.0;
    d.rhos          = 1.0;
    % viscosity and stiffness parameter
    d.muf           = 1.0e-2;
    d.mus           = 1.0e-1;
    % angular frequency
    d.w             = 2.0 * pi / 1.024;
    % inner / outer radii
    d.Hi            = 1.0;
    d.Ho            = 1.2;
    % length of domain
    d.L             = 1.0;
    % final time
    d.T             = 2.0 * pi / d.w;
    % number of time steps for evaluation
    d.nt            = 1024;

    % transient or quasi-static cases
    d.dynamicF      = true;
    d.dynamicS      = true;
    % linear or nonlinear cases
    d.nonlinear     = false;
    % 2D or 3D
    d.nd            = 2;
    % don't check balance equation
    d.balanceEqn    = false;
    d.nSamples      = 100;
    d.balanceFtol   = 1.0e-4;
    d.balanceStol   = 1.0e-4;
    d.constrTtol    = 1.0e-8;
    d.constrVtol    = 1.0e-8;

    custom_colormap;
    d.colormap      = mycolormap;

    % visualize using the right half of the screen
    d.viewport      = [0.5 0 0.5 1];
    d.manyt         = false;
    % don't export movie by default
    d.movie_export  = false;

    d.initialized   = true;

end