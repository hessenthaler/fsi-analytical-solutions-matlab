%% evaluate and visualize analytic FSI solutions
%
%  author: Andreas Hessenthaler, University of Stuttgart
%
%  runpath: fsi-analytical-solutions-matlab

%% clean house and modify Matlab defaults
clc;
close all;
clear all;

%% modify path
addpath(genpath('src/'));

%% modify Matlab defaults (e.g., use LaTeX fonts)
modify_matlab_defaults;

%% set all variables to defaults
data    = set_default_parameters();

%% user-functions for customizing setup
data    = edit_parameters(data, 'number of dimensions',             3);
data    = edit_parameters(data, 'user mesh',                        false);
data    = edit_parameters(data, 'final time',                       1.024);
data    = edit_parameters(data, 'fluid density',                    2.1);
data    = edit_parameters(data, 'solid density',                    1.0);
data    = edit_parameters(data, 'pressure amplitude',               1.0);
data    = edit_parameters(data, 'fluid viscosity',                  3.0e-2);
data    = edit_parameters(data, 'solid stiffness',                  1.0e-1);
data    = edit_parameters(data, 'inner radius',                     0.7);
data    = edit_parameters(data, 'outer radius',                     1.0);
data    = edit_parameters(data, 'number of time steps',             20);
data    = edit_parameters(data, 'length',                           1.0);
data    = edit_parameters(data, 'dynamic fluid',                    true);
data    = edit_parameters(data, 'dynamic solid',                    true);
data    = edit_parameters(data, 'nonlinear',                        true);
% set a flag to enable checking of the momentum balance, etc.
data    = edit_parameters(data, 'check balance equations',          true);
data    = edit_parameters(data, 'number of samples (balance eqn.)', 100);
data    = edit_parameters(data, 'tolerance (balance eqn. - fluid)', 1.0e-4);
data    = edit_parameters(data, 'tolerance (balance eqn. - solid)', 1.0e-4);
data    = edit_parameters(data, 'tolerance (traction constr.)',     1.0e-8);
data    = edit_parameters(data, 'tolerance (velocity constr.)',     1.0e-8);
% some additional parameters regarding data export and visualization
data    = edit_parameters(data, 'export movie',                     false);
data    = edit_parameters(data, 'visualize multiple time steps',    false);
data    = edit_parameters(data, 'export to folder',                 'data/');

%% read coordinates or define mesh import routine
data    = get_default_mesh(data);
% user-defined mesh import
if data.userMesh
    % implement user-defined mesh import routine here
    error('Please implement a user-mesh import routine or set data.defaultMesh to true.');
end

%% evaluate analytic solution
data    = get_solution_function_handles(data);
data    = evaluate_analytic_solution(data);

%% print parameters
print_parameters(data);

%% check momentum balance, etc.
check_momentum_balance_and_constraints(data);

%% visualize analytic solution on xy-plane
% set viewport, i.e., which space the Matlab figure should reside in
% using normalized coordinates
% for example, the following viewport causes the figure to use the right
% half of the screen
viewport    = [0.5 0 0.5 1];
data        = set_viewport(data, viewport);
data        = visualize_analytic_solution(data);

%% export analytic solution
export_data(data);

%% we're done
fprintf('Program finished.\n');