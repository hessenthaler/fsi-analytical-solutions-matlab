# Matlab code for analytic FSI solutions #

This repository contains Matlab code that was written as *Supplementary Material* for the following preprint:

* [**Hessenthaler, Balmus, R�hrle, Nordsletten**: *"A Class of Analytic Solutions for Verification and Convergence Analysis of Linear and Nonlinear Fluid-Structure Interaction Algorithms"*](https://arxiv.org/abs/1906.04033)

Note: The code was written for **Matlab 9.3.0.713579 (R2017b)** and some functionality may not be available for **Matlab 2016a** or older.

### How do I get set up? ###

Download the code or use git to get a copy of this repository:

* git clone https://bitbucket.org/hessenthaler/fsi-analytical-solutions-matlab.git

### How do I run the code? ###

Run the file [main.m](main.m) in Matlab.

### How do I edit parameters? ###

In [main.m](main.m), find and edit the following lines:

```
    data    = edit_parameters(data, 'number of dimensions',             3);
    data    = edit_parameters(data, 'user mesh',                        false);
    data    = edit_parameters(data, 'final time',                       1.024);
    data    = edit_parameters(data, 'fluid density',                    2.1);
    data    = edit_parameters(data, 'solid density',                    1.0);
    data    = edit_parameters(data, 'pressure amplitude',               1.0);
    data    = edit_parameters(data, 'fluid viscosity',                  3.0e-2);
    data    = edit_parameters(data, 'solid stiffness',                  1.0e-1);
    data    = edit_parameters(data, 'inner radius',                     0.7);
    data    = edit_parameters(data, 'outer radius',                     1.0);
    data    = edit_parameters(data, 'number of time steps',             20);
    data    = edit_parameters(data, 'length',                           1.0);
    data    = edit_parameters(data, 'dynamic fluid',                    true);
    data    = edit_parameters(data, 'dynamic solid',                    true);
    data    = edit_parameters(data, 'nonlinear',                        true);
    data    = edit_parameters(data, 'check balance equations',          true);
    data    = edit_parameters(data, 'number of samples (balance eqn.)', 100);
    data    = edit_parameters(data, 'tolerance (balance equation)',     1.0e-4);
    data    = edit_parameters(data, 'tolerance (constraints)',          1.0e-8);
    data    = edit_parameters(data, 'export movie',                     false);
    data    = edit_parameters(data, 'visualize multiple time steps',    false);
    data    = edit_parameters(data, 'export to folder',                 'data/');
```

### Exporting the analytic solutions ###

By default, the evaluated solutions are exported to [data/](data/).

### Example visualization ###

Movies of the animated solutions can be found in [movies/](movies/).

Movies for parameters in the manuscript are also embedded on the [Wiki pages](https://bitbucket.org/hessenthaler/fsi-analytical-solutions-matlab/wiki/Home).

### Verifying analytic solutions ####

The analytic solutions are verified using numerical differentiation, see [src/check_momentum_balance_and_constraints](src/check_momentum_balance_and_constraints.m).

### Who do I talk to? ###

* Andreas Hessenthaler, University of Stuttgart

### License ###

This repository is distributed under the terms of both the MIT license and the Apache License (Version 2.0). Users may choose either license, at their option.

All new contributions must be made under both the MIT and Apache-2.0 licenses.

See [LICENSE-MIT](LICENSE-MIT), [LICENSE-APACHE](LICENSE-APACHE) and [COPYRIGHT](COPYRIGHT) for details.
