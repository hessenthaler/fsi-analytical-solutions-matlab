#!/bin/bash

for file in `ls *.mp4`
do
	fname=`basename $file .mp4`
	ffmpeg -i $fname.mp4 $fname.gif
done
